import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeRoutingModule } from './modules/home/home-routing.module';
import { SearchRoutingModule } from './modules/search/search-routing.module';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { SelectivePreloadingStrategyService } from './shared/services/selective-preloading-strategy.service';
import { MovieRoutingModule } from './modules/movie/movie-routing.module';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full',  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        preloadingStrategy: SelectivePreloadingStrategyService,
      }
      ),
    HomeRoutingModule,
    SearchRoutingModule,
    MovieRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
