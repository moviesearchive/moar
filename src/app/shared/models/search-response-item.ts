export class SearchResponseItem {
    Poster: string;
​​​    Title: string;
    ​​​Type: string;
​    Year: string;
​​    imdbID: string;
    plot?: string;
    rating?: string;
}
