import { SearchResponseItem } from './search-response-item';

export class SearchResponse {
    Response: string;
    Search: Array<SearchResponseItem>;
    Error?: string;
}
