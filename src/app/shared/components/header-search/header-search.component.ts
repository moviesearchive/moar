import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header-search',
  templateUrl: './header-search.component.html',
  styleUrls: ['header-search.component.scss']
})
export class HeaderSearchComponent implements OnInit {
  logoSize = 60;
  form: FormGroup;
  currentRouteTitle = this.route.snapshot.paramMap.get('q');
  currentRouteGenre = this.route.snapshot.paramMap.get('g') || '';

  genres: Genre[] = [
    { value: '', viewValue: 'All' },
    { value: 'movie', viewValue: 'Movie' },
    { value: 'series', viewValue: 'Series' },
    { value: 'game', viewValue: 'Game' },
    { value: 'episode', viewValue: 'Episode' }
  ];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      text: this.currentRouteTitle,
      genre: this.currentRouteGenre
    });
  }

  submitForm() {
    const formTitle: string = this.form.get('text').value;
    const formGenre: string = this.form.get('genre').value;
    if (formTitle.length && (this.currentRouteTitle !== formTitle || this.currentRouteGenre !== formGenre)) {
      this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
        if (!formGenre.length) {
          this.router.navigate( ['search', { q: formTitle } ]);
        } else {
          this.router.navigate( ['search', { q: formTitle, g: formGenre } ]);
        }
      });
    };
  } 
}

export interface Genre {
  value: string;
  viewValue: string;
}
