import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rated-image',
  templateUrl: './rated-image.component.html',
  styleUrls: ['rated-image.component.scss']
})
export class RatedImageComponent implements OnInit {
  @Input() imageSource: string;
  @Input() size: string;
  @Input() rating: string;

  constructor() {}

  ngOnInit() {}
}
