import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';

import { ApiService } from 'src/app/core/services/api.service';
import { MovieEdited } from 'src/app/shared/models/movie-edited';
import { Rating } from 'src/app/shared/models/rating';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['movie.component.scss']
})
export class MoviePageComponent implements OnInit, OnDestroy {
  data: MovieEdited;
  logoSize = 10;
  posterSize = 220;
  posterUrl: string;
  directorText = "Director: ";
  searchTitle = 'We found some: '
  dataLoaded = false;
  showModal: boolean;
  castArray: string[];
  ratingsArray: Rating[];

  private onDestroy$: Subject<void> = new Subject();

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.dataLoaded = false;
    let imdbId = this.route.snapshot.paramMap.get('imdbId');
    if (imdbId === null || undefined) { imdbId = '' }
    
    if (imdbId.length) {
      this.apiService.getMovieById(this.onDestroy$, imdbId, (movieData: MovieEdited) => {
        console.log(movieData);
        this.data = movieData;
        this.castArray = this.getCast();
        this.ratingsArray = this.getRatings();
        this.getPoster();
        if (this.data.Director.length > 1) this.directorText = "Directors: "
      });
    };
    this.dataLoaded = true;
  }

  getPoster() {
    if (this.data.Poster !== "N/A") {
      this.posterUrl = this.data.Poster;
    } else {
      this.posterUrl = 'assets/img/posterNotFound.jpg'
    }
  }

  getCast(): string[] {
    let cast: string[] = [];
    if (this.data.Actors.length && this.data.Actors[0] !== "N/A") {
      cast = [...this.data.Actors];
    }
    if (this.data.Writer.length && this.data.Writer[0] !== "N/A") {
      cast = [...cast, ...this.data.Writer]
    }
    
    return cast;
  }

  getRatings(): Rating[] {
    let ratingsArr: Rating[] = [];
    if (this.data.Ratings.length) {
      this.data.Ratings.map((rating) => {
        ratingsArr = [ ...ratingsArr, rating]
      });
    }

    return ratingsArr;
  }

  enlargePoster() {
    this.showModal = true;
  }

  hideEnlargedPoster() {
    this.showModal = false;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}
