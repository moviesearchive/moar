import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-adnotated-image',
  templateUrl: './adnotated-image.component.html',
  styleUrls: ['adnotated-image.component.scss']
})
export class AdnotatedImageComponent implements OnInit {
  @Input() adnotation: string;
  
  posterUrl = 'assets/img/posterNotFound.jpg';
  posterSize = 50;

  constructor() {}

  ngOnInit() {}
}
