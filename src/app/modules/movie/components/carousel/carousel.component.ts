import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  @Input() data: string[];
  slides: Array<Array<string>> = [[]];
  singleGroup: string[] = [];
  currentGroup = 0;

  constructor() {}

  ngOnInit() {
    this.slides = this.chunk(this.data, 4);
    this.populateSingleGroup();
  }

  chunk(array: string[], chunkSize: number) {
    let group = [];
    if (array.length <= chunkSize) {
      group.push([...array]);
      return group;
    }
    for (let i = 0, len = array.length; i < len; i += chunkSize) {
      group.push(array.slice(i, i + chunkSize));
    }
    return group;
  }

  populateSingleGroup() {
    if (this.slides.length) {
      this.singleGroup = this.slides[this.currentGroup];
      return this.singleGroup;
    }
  }

  nextGroup() {
    if (this.currentGroup < this.slides.length - 1) {
      this.currentGroup++;
      this.populateSingleGroup();
    }
  }

  previouseGroup() {
    if (this.currentGroup > 0) {
      this.currentGroup--;
      this.populateSingleGroup();
    }
  }
}
