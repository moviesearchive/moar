import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['rating.component.scss']
})
export class RatingComponent implements OnInit {
  @Input() providerName: string;
  @Input() ratingValue: string;
  @Input() special = false;

  fillingWidth: number = 0;

  constructor() {}

  ngOnInit() {
    this.calculateFillingsWidth();
  }

  calculateFillingsWidth() {
    this.fillingWidth = 7.5/10 * Number.parseFloat(this.ratingValue);
    if (this.fillingWidth === NaN) { this.fillingWidth = 0 };
  }

}
