import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { 
  MatFormFieldModule, 
  MatIconModule, 
  MatInputModule } from '@angular/material';

import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core/core.module';
import { MoviePageComponent } from './pages/movie/movie.component';
import { AdnotatedImageComponent } from './components/adnotated-image/adnotated-image.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { RatingComponent } from './components/rating/rating.component';


@NgModule({
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        FormsModule,
        SharedModule,
        CoreModule,
      ],
      declarations: [
        MoviePageComponent,
        AdnotatedImageComponent,
        CarouselComponent,
        RatingComponent
      ]
})
export class MovieModule { }
