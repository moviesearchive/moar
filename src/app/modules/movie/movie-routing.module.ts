import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MoviePageComponent } from './pages/movie/movie.component';

const routes: Routes = [
  {
    path: 'movie',
    component: MoviePageComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class MovieRoutingModule { }