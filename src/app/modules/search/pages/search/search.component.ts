import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';

import { SearchResponseItem } from 'src/app/shared/models/search-response-item';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['search.component.scss']
})
export class SearchPageComponent implements OnInit, OnDestroy {
  data: SearchResponseItem[] = [];
  logoSize = 10;
  searchTitle = 'We found some: '
  dataLoaded = false;
  pageCounter = 1;

  private onDestroy$: Subject<void> = new Subject();

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.loadData();
    
  }

  loadData() {
    const question = this.route.snapshot.paramMap.get('q') || '';
    const genre = this.route.snapshot.paramMap.get('g') || '';
    
    if (question.length) {
      this.apiService.searchForMovie(this.onDestroy$, question, genre, this.pageCounter,
        (moviesList: SearchResponseItem[]) => {
        
        if (this.data) {
          this.data = [...this.data, ...moviesList];
        } else {
          this.data = [...moviesList];
        }
        this.pageCounter++;
        this.dataLoaded = true;
      });
    } else {
      this.dataLoaded = true;
    }
  }

  onScrolled() {
    if (this.dataLoaded) {
      this.loadData();
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}
