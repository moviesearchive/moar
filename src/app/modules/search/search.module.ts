import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { 
  MatFormFieldModule, 
  MatIconModule, 
  MatInputModule } from '@angular/material';

import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core/core.module';
import { SearchPageComponent } from './pages/search/search.component';
import { ContentContainerComponent } from './components/content-container/content-container.component';
import { SearchItemComponent } from './components/search-item/search-item.component';
import { InfiniteScrollComponent } from './components/infinite-scroll/infinite-scroll.component';


@NgModule({
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        FormsModule,
        SharedModule,
        CoreModule,
      ],
      declarations: [
        SearchPageComponent,
        ContentContainerComponent,
        SearchItemComponent,
        InfiniteScrollComponent
      ]
})
export class SearchModule { }
