import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

import { SearchResponseItem } from 'src/app/shared/models/search-response-item';
import { ApiService } from 'src/app/core/services/api.service';
import { Movie } from 'src/app/shared/models/movie';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-item',
  templateUrl: './search-item.component.html',
  styleUrls: ['search-item.component.scss']
})
export class SearchItemComponent implements OnInit, OnDestroy {
  @Input() data: SearchResponseItem;

  posterSize = 150;
  posterUrl: string;
  ratingText: string;
  plotLoaded = false;

  private onDestroy$: Subject<void> = new Subject();

  constructor(
    private apiService: ApiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getPoster();
  }

  getRatingText() {
    if (this.data.rating) {
      this.ratingText = this.data.rating;
    } else {
      this.ratingText = `Not rated`;
    }
  }

  getPoster() {
    if (this.data.Poster !== "N/A") {
      this.posterUrl = this.data.Poster;
    } else {
      this.posterUrl = 'assets/img/posterNotFound.jpg'
    }
    this.getPlotAndRating();
  }

  getPlotAndRating() {
    this.plotLoaded = false;
    this.apiService.getMovieRatingShortPlot(this.onDestroy$, this.data.imdbID, 
      (moviePlotData: string, movieRatingData: string) => {
        moviePlotData ? this.data.plot = moviePlotData : this.data.plot = 'Could not find plot.';
        movieRatingData ? this.data.rating = movieRatingData : this.data.rating = 'N/A';
        this.plotLoaded = true;
        this.getRatingText();
    });
  }

  navigateToMovie() {
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate( ['movie', { imdbId: this.data.imdbID }, ]);
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
