import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';

import { Router } from '@angular/router';

@Component({
  selector: 'app-home-search',
  templateUrl: './home-search.component.html',
  styleUrls: ['home-search.component.scss']
})
export class HomeSearchComponent implements OnInit, OnDestroy {
  form: FormGroup;
  private onDestroy$: Subject<void> = new Subject();

  constructor(
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      text: ''
    });
  }

  submitForm() {
    const formVal: string = this.form.get('text').value;
    if (formVal.length) {
      this.router.navigate( ['search', { q: formVal }]);
    };
  } 

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}
