import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { 
  MatFormFieldModule, 
  MatIconModule, 
  MatInputModule } from '@angular/material';

import { HomePageComponent } from './pages/home/home.component';
import { HomeSearchComponent } from './components/home-search/home-search.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core/core.module';


@NgModule({
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        FormsModule,
        SharedModule,
        CoreModule,
      ],
      declarations: [
        HomePageComponent,
        HomeSearchComponent,
      ]
})
export class HomeModule { }
