import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CoreModule } from '../core.module';

// Security 3.5 Ask for file nearest Movie Archive admin
import { API_KEY } from './api'


@Injectable({
  providedIn: CoreModule,
})
export class BaseService {
    private API_KEY = API_KEY;
    private apiPrefix = `http://www.omdbapi.com/`;
    private apiSuffix = `&apikey=${this.API_KEY}`;

    constructor( private http: HttpClient ){}

    /**
     * Builds whole URL with prefix and suffix
     * @param restOfUrl actual data for API
     */
    protected apiUrl(restOfUrl: string) {
        return this.apiPrefix + restOfUrl + this.apiSuffix;
    }

    protected get(path: string) {
        return this.http.get(this.apiUrl(path));
    }
}