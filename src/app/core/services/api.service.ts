import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CoreModule } from '../core.module';
import { Movie } from 'src/app/shared/models/movie';
import { Rating } from 'src/app/shared/models/rating';

import { BaseService } from './base.service';
import { SearchResponse } from 'src/app/shared/models/search-response';
import { SearchResponseItem } from 'src/app/shared/models/search-response-item';
import { MovieEdited } from 'src/app/shared/models/movie-edited';


@Injectable({
  providedIn: CoreModule,
})
export class ApiService extends BaseService {
    static movieData: MovieEdited;
    static movieDataArray: Movie[];
    static moviesList: Array<SearchResponseItem>;

    constructor( http: HttpClient ) { super(http) }

    getMovieByTitle(
        onDestroy$: Subject<void>,
        data: string, callback: (movieData: Movie) => void) {
        const serializedTitle = this.serializeStringToUrl(data);

        return this.get(`?t=${serializedTitle}`)
            .pipe(takeUntil(onDestroy$))
            .subscribe((movie: Movie) => {
                callback(movie);
            });
    }

    getMovieById( onDestroy$: Subject<void>,
        imdbId: string, callback: (movieData: MovieEdited) => void) {

        return this.get(`?i=${imdbId}&plot=full`)
            .pipe(takeUntil(onDestroy$))
            .subscribe((movie: Movie) => {
                const newMovie: MovieEdited = this.serializeMovie(movie);
                ApiService.movieData = newMovie;
                callback(newMovie);
            });
    }

    searchForMovie(
        onDestroy$: Subject<void>,
        question: string, genre: string, page: number, 
        callback: (movieList: SearchResponse | Array<SearchResponseItem>) => void) {
        const checkedValues = this.checkForYear(question);
        
        const productionYear = checkedValues[1];
        const serializedTitle = this.serializeStringToUrl(checkedValues[0]);
        
        return this.get(`?s=${serializedTitle}&page=${page}&type=${genre}&y=${productionYear}`)
        .pipe(takeUntil(onDestroy$))
        .subscribe((movieSearchData: SearchResponse) => {
            if (movieSearchData.Response === 'True') {
                ApiService.moviesList = this.extractSearchResponse(movieSearchData);
                return callback(ApiService.moviesList);
            }
            return callback(this.extractSearchResponseFailed(movieSearchData));
        });
    }

    getMovieRatingShortPlot(
        onDestroy$: Subject<void>,
        imdbId: string, callback: (moviePlot: string, movieRating: string) => void) {  
        
        return this.get(`?i=${imdbId}`)
            .pipe(takeUntil(onDestroy$))
            .subscribe((movieShortPlotData: Movie) => callback(movieShortPlotData.Plot, movieShortPlotData.imdbRating));
    }

    /**
     * Replaces string to match API request.
     * *Changes " " to "+" and forces lower case
     * @param value string 
     */
    private serializeStringToUrl(value: string) {
        value = value.trim();
        return value.replace(/ /g, "+");
    }
  
    /**
     * Extracts response data to provide an array
     * @param data SearchResponse
     */
    private extractSearchResponse = (data: SearchResponse): Array<SearchResponseItem> => {
        return data.Search;
    }

    private extractSearchResponseFailed = (data: SearchResponse): Array<SearchResponseItem> => {
        return [];
    }

    private extractStringToArray = (director: string): string[] => {
        const directors = director.split(', ');
        return directors;
    }

    private serializeMovie (data: Movie): MovieEdited {
        const movie = new MovieEdited();
        movie.Awards = data.Awards;
        movie.Actors = this.extractStringToArray(data.Actors);
        movie.BoxOffice = data.BoxOffice;
        movie.Country = data.Country;
        movie.DVD = data.DVD;
        movie.Director = this.extractStringToArray(data.Director);
        movie.Genre = data.Genre;
        movie.Language = data.Language;
        movie.Metascore = data.Metascore;
        movie.Plot = data.Plot;
        movie.Poster = data.Poster;
        movie.Production = data.Production;
        movie.Rated = data.Rated;
        movie.Ratings = this.serializeRatings(data.Ratings);
        movie.Released = data.Released;
        movie.Response = data.Response;
        movie.Runtime = data.Runtime;
        movie.Title = data.Title;
        movie.Type = data.Type;
        movie.Website = data.Website;
        movie.Writer = this.extractStringToArray(data.Writer);
        movie.Year = data.Year;
        movie.imdbID = data.imdbID;
        movie.imdbRating = data.imdbRating;
        movie.imdbVotes = data.imdbVotes;

        return movie;
    }

    private serializeRatings (ratings: Rating[]): Rating[] {
        const newRatings = new Array<Rating>();
        ratings.map((rating) => {
            const newRating: Rating = {
                Value: this.unifyRatingPattern(rating.Value),
                Source: rating.Source
            };
            newRatings.push(newRating);
        });

        return newRatings;
    }

    private unifyRatingPattern = (value: string): string => {
        switch(true) {
            // matches for "8.7/10"    
            case /\d+\.+\d+(?:\/\d+)/.test(value):
                value.length === 5 ? value = '10.0' : value = `${value.substr(0, 3)}`;
                break;
            // matches for "73/100"
            case /\d+(?:\/\d+)*$/.test(value):
                value.length === 7 ? value = '10.0' : value = `${value.substr(0, 1)}.${value.substr(1, 1)}`;
                break;    
            // matches for "88%"    
            case /\d+(?:\%)/.test(value):
                value.length === 4 ? value = '10.0' : value = `${value.substr(0, 1)}.${value.substr(1, 1)}`;
                break;
            // match for "8.7" or any other   
            default: value;
                break;
        }
        return value;
    }

    private checkForYear = (value: string): string[] => {
        const regEx = /([0-9]{4})/g;
        const regExSearch = regEx.exec(value);
        if (regExSearch && value.length > 4) {
            return [value.replace(regEx, ''), regExSearch[0]];
        }
        return [value, ''];
    }
}
